package masonwan.demo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResponse {
    private String asin;
    private long nextOffset;
    private List<Product> items;
}
