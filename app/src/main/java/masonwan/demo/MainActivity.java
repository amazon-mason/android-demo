package masonwan.demo;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ProgressDialog progressDialog;
    private LinearLayoutManager linearLayoutManager;
    private ProductViewAdapter adapter;
    private RecyclerView recyclerView;
    private ObjectMapper objectMapper = new ObjectMapper();
    private StringRequest stringRequest;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutManager = new LinearLayoutManager(this);
        adapter = new ProductViewAdapter(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading products...");
        progressDialog.show();

        stringRequest = new StringRequest(
                Request.Method.GET,
                "http://masonwan.com:3000/products",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            ProductResponse productResponse = objectMapper.readValue(response, ProductResponse.class);
                            Log.d(TAG, String.format("Got %d items", productResponse.getItems().size()));

                            recyclerView.invalidate();
                            adapter.setProducts(productResponse.getItems());

                            progressDialog.dismiss();

                        } catch (IOException e) {
                            Log.e(TAG, String.format("Failed to parse JSON: '%s'", response), e);
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Failed to get JSON", error);
                    }
                }
        );
        Singletons.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (stringRequest != null) {
            stringRequest.cancel();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

//        outState.put
    }
}
