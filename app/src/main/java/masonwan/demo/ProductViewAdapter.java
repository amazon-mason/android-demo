package masonwan.demo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;

class ProductViewAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Product> products = new ArrayList<>();

    public ProductViewAdapter(Context context) {
        this.context = context;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Product product = products.get(position);

        CardView cardView = (CardView) holder.itemView;
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(String.format("http://amazon.com%s", product.getClickUrl()));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(browserIntent);
            }
        });

        NetworkImageView networkImageView = (NetworkImageView) cardView.findViewById(R.id.networkImageView);
        ImageLoader imageLoader = Singletons.getInstance(context).getImageLoader();
        networkImageView.setImageUrl(product.getImageUrl(), imageLoader);

        TextView textView = (TextView) cardView.findViewById(R.id.title);
        textView.setText(product.getTitle());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}

class ViewHolder extends RecyclerView.ViewHolder {
    public ViewHolder(View itemView) {
        super(itemView);
    }
}