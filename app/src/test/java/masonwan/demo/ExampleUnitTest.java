package masonwan.demo;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertThat(2 + 2)
                .isEqualTo(4);
    }
}
